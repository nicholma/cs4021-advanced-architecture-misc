require 'graphviz'


5.times do |i|
  puts i
end



# Create a new graph
g = GraphViz.new( :G, :type => :digraph )

# Create two nodes
hello = g.add_nodes( "Hello" )
world = g.add_nodes( "World" )
other = g.add_nodes( "!" )
otherother = g.add_nodes( "!" )

# Create an edge between the two nodes
g.add_edges( hello, world )
g.add_edges( hello, other )
g.add_edges( hello, otherother )

# Generate output image
#g.output( :png => "hello_world.png" )