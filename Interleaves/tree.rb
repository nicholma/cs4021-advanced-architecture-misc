require 'graphviz'

class StateGraphNM
# Create a new graph
  def initialize (_m, _n)
    @g = GraphViz.new( :G, :type => :digraph )
    @m = _m
    @n = _n
    @node_count =1
    @interleaves = 0
    i_string= '1\n'
    i_states = []

    @n.times do |k|
      i_states << 0
      i_string += "p#{k}:s0\n"
    end     # end for
    
    puts i_string
    root = @g.add_nodes(i_string)
    add_entry(i_states, root)
    puts "Number of interleaves: #{@interleaves}"
    puts 'Calc complete. drawing graph...'
    @g[:label] = "Processes: #{@n}- Statements: #{@m}\nNumber of Interleaves: #{@interleaves}"
    @g.output( :svg => "p#{@n}-s#{@m}_I#{@interleaves}.svg")
    
  end #end init

  def add_entry(current_state, parent)
    @node_count += 1
    @n.times do |i|
      if current_state[i] < @m
        current_state[i] += 1
        string = (@node_count.to_s) + "\n"
        test = true
        @n.times do |j|
          if current_state[j] != @m
            test =false
          end
          string += "p#{j}:s#{current_state[j]}\n"
        end #end for j
        
        if test
          @interleaves += 1
        end
          
        puts string
        node = @g.add_nodes(string)
        @g.add_edges(parent, node)
        add_entry(current_state, node)
        current_state[i] -= 1
      end  #end if state 
    end #end for i
  end #end add_entry


end #end StateGraphNM

m = ARGV.shift
if m == nil
  print "Enter number of statements: "
  m = gets.chomp
end

n = ARGV.shift
if n == nil
  print "Enter number of processes: "
  n = gets.chomp    
end

m = m.to_i
n = n.to_i

StateGraphNM.new(m,n)
