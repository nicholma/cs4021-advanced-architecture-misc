#require 'graphviz'

class StateGraphNM
  def initialize (_m, _n)
    @m = _m
    @n = _n
    @node_count =1
    @interleaves = 0
    i_states = []

    @n.times do |k|
      i_states << 0
    end     # end for
    
    add_entry(i_states)
    puts "Number of interleaves: #{@interleaves}"    
  end #end init

  def add_entry(current_state)
    @node_count += 1
    @n.times do |i|
      if current_state[i] < @m
        current_state[i] += 1

        test = true
        @n.times do |j|
          if current_state[j] != @m
            test =false
          end
        end #end for j
        
        if test
          @interleaves += 1
        end
          
        add_entry(current_state)
        current_state[i] -= 1
      end  #end if state 
    end #end for i
  end #end add_entry


end #end StateGraphNM

m = ARGV.shift
if m == nil
  print "Enter number of statements: "
  m = gets.chomp
end

n = ARGV.shift
if n == nil
  print "Enter number of processes: "
  n = gets.chomp    
end

m = m.to_i
n = n.to_i

StateGraphNM.new(m,n)